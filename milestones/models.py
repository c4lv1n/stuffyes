from django.db import models
from datetime import datetime
from tasks.models import Task
from assignments.models import Assignment


class Milestone(models.Model):
    assignment = models.ForeignKey(Assignment, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    deadline = models.DateTimeField()
    progress = models.DecimalField(max_digits=3, decimal_places=1, default=0.0)
    complete = models.BooleanField(default=False)
    tasks_to_complete = models.ManyToManyField(Task)

    def __str__(self):
        return self.name
