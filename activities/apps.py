from django.apps import AppConfig


class ActivitiesConfig(AppConfig):
    name = 'activities'


class ActivityLogsConfig(AppConfig):
    name = 'activitylogs'
