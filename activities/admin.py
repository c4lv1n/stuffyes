from django.contrib import admin
from .models import Activity
from .models import ActivityLogs

class ActivityLogsAdmin(admin.ModelAdmin):
    list_display = ('activity_id', 'name', 'contribution', 'time_taken', 'time_complete')
    list_display_links = ('activity_id', 'name')
    list_filter = ('activity_id',)

    list_per_page = 25



class ActivityAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'contribution', 'assignment')
    list_display_links = ('id', 'name')
    list_filter = ('assignment',)

    list_per_page = 25


admin.site.register(Activity, ActivityAdmin)
admin.site.register(ActivityLogs, ActivityLogsAdmin)