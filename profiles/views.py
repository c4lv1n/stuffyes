import calendar
from datetime import datetime, timedelta
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from .models import Profile
from modules.models import Module
from assignments.models import Assignment
from tasks.models import Task
from activities.models import Activity
from activities.models import ActivityLogs
from milestones.models import Milestone

from constants.colours import MODULE_COLOURS, MODULE_STYLES, MODULE_ICONS
from constants.types import STUDY_TYPES


@login_required(login_url='login')
def dashboard(request, user_id):
    user = get_object_or_404(User, pk=user_id)

    # Make sure user has authority to access the page
    if auth.get_user(request).id == user.id:
        # POST REQUEST
        if request.method == 'POST':
            title = request.POST['title']
            modules = request.POST['modules']

            # Get individual module titles
            unique_modules = []
            for mod in str(modules).split(', '):
                if not unique_modules.__contains__(mod.lower()):
                    unique_modules.append(mod)

            # Create profile
            new_profile = Profile.objects.create(account=user, title=title)

            # Add modules
            for mod in unique_modules:
                new_profile.add_module(mod)

            # return redirect('/profiles/{}'.format(user.id))

        # GET REQUEST

        # Get study profiles
        profiles = Profile.objects.filter(account=user)

        # Get modules
        modules = None

        if profiles:
            modules = [mod for mod in Module.objects.all()
                       if mod.profile in profiles]

            # Assign module colours
            for mod in modules:
                mod.colour = MODULE_COLOURS[modules.index(
                    mod) % len(MODULE_COLOURS)]

                mod.style = MODULE_STYLES[modules.index(
                    mod) % len(MODULE_STYLES)]

            # Assign module icons
            for mod in modules:
                mod.icon = MODULE_ICONS[modules.index(
                    mod) % len(MODULE_ICONS)]

        # Get assignments
        assignments = None
        if modules:
            assignments = [assignment for assignment in Assignment.objects.all(
            ) if assignment.module in modules]

        # Get tasks
        tasks = None
        if assignments:
            tasks = [task for task in Task.objects.all(
            ) if task.assignment in assignments]

            # Add extra field to task for easier templating
            for task in tasks:
                task.profile = task.assignment.module.profile
                task.colour = next(mod.colour for mod in modules if (
                    task.assignment.module == mod))
                task.style = next(mod.style for mod in modules if (
                    task.assignment.module == mod))



        # Get started activities
        activities = [activity for activity in ActivityLogs.objects.all()]




        # Dates for the calendar
        dates = []
        current_month = datetime.now().month
        current_date = datetime(datetime.now().year, current_month, 1)

        for i in range(5):
            month = {}
            current_date = datetime(current_date.year, current_month, 1)
            current_date -= timedelta(datetime.weekday(current_date))

            month.update({'row0': ['x', 'x', 'x', 'x', 'x', 'x', 'x']})
            month.update({'row1': ['x', 'x', 'x', 'x', 'x', 'x', 'x']})
            month.update({'row2': ['x', 'x', 'x', 'x', 'x', 'x', 'x']})
            month.update({'row3': ['x', 'x', 'x', 'x', 'x', 'x', 'x']})
            month.update({'row4': ['x', 'x', 'x', 'x', 'x', 'x', 'x']})

            for key in month.keys():
                for i in range(7):
                    month[key][i] = current_date
                    # Increment the date
                    current_date += timedelta(days=1)

            month.update({'year': current_date.year})
            month.update({'month': calendar.month_name[current_month]})
            dates.append(month)
            # set the new month
            current_month = (current_date + timedelta(days=1)).month

        context = {
            'user': user,
            'profiles': profiles,
            'modules': modules,
            'assignments': assignments,
            'tasks': tasks,
            'dates': dates,
            'types': STUDY_TYPES,
            'activities': activities
        }

        return render(request, 'profiles/dashboard.html', context)
    else:
        auth.logout(request)
        return redirect('login')


def doactivity(request, act_name):


    contribution = None
    time_taken = None

    activity = get_object_or_404(Activity, name = act_name)

    tasks = activity.associated_tasks.all()
 #   tasks = Task.objects.all()
    print(tasks)

    # POST REQUEST
    if request.method == 'POST':

        # save to logs
        contribution = int(request.POST['contribution'])
        time_taken = int(request.POST['time_taken'])
        activity_id = Activity.objects.get(name=act_name)
        a = ActivityLogs(activity_id = activity_id, name=act_name, time_taken=time_taken, contribution=contribution)
        a.save()


        # increment time dedicated to activity as well as contribution to tasks
        actToSave = Activity.objects.get(name = act_name)
        actToSave.do_activity(time_taken, contribution)



    context = {
        'activity': act_name,
        'tasks': tasks
    }

    return render(request, 'profiles/doActivityModal.html', context)



def viewactivity(request):
    name = "a5"






    # data to pass onto the web page
    context = {
        'activity': name
    }



    return render(request, 'profiles/addActivityModal.html', context)




def addactivity(request):
    from constants.types import STUDY_TYPES


    # NEED TO ADD BASED UPON ASSIGNMENT



    # boolean value to determine that the study types match
    typesMatch = True


    #get activities
    activities = [activity for activity in Activity.objects.all()]

    # Get tasks
    tasks = [task for task in Task.objects.all()]






    #POST REQUEST
    if request.method == 'POST':

        name = request.POST['name']
        contribution = int(request.POST['contribution'])
        time_taken = request.POST['time_taken']

        if(time_taken == ''):
            print("hi")
            time_taken = 0

        # only returns selected tasks
        got_tasks = request.POST.getlist('task')

        # only returns the selected study type
        got_type = request.POST.getlist('atype')


        # ensure that the study type matches the types of the tasks

        for task_id in got_tasks:

            task_from_id = Task.objects.get(id=task_id)

            if(got_type[0].upper() != task_from_id.study_type):
                typesMatch = False


        # add activity information to database
        if(typesMatch is True):
            a = Activity(assignment = Assignment.objects.get(title="asdasd"), name = name, contribution = contribution, time_taken = time_taken,\
                         study_type = got_type[0].upper())
            a.save()


            for task_id in got_tasks:
                task_from_id = Task.objects.get(id=task_id)
                a.associated_tasks.add(task_from_id)

            print(a)








    # data to pass onto the web page
    context = {
        'activities': activities,
        'tasks': tasks,
        'types': STUDY_TYPES,
        'validateType': typesMatch
    }



    return render(request, 'profiles/addActivityModal.html', context)



# adding a milestone
def addmilestone(request):
    tasks = [task for task in Task.objects.all()]

    # boolean value to determine if deadline is ahead of tasks to complete deadline
    deadlineValid = True


    if request.method == "POST":
        name = request.POST['name']
        got_tasks = request.POST.getlist('task')

        date = request.POST['date']
        time = request.POST['time']

        concat = date + ' ' + time
        formatDateTime = datetime.strptime(concat, '%Y-%m-%d %H:%M')



        task_count = 0

        # iterate through all tasks
        for task_id in got_tasks:
            task_from_id = Task.objects.get(id=task_id)
            task_from_id.end_date = task_from_id.end_date.replace(tzinfo = None)


            if task_from_id.end_date.replace(tzinfo = None)> formatDateTime:
                deadlineValid = False

            task_count += 1


        if deadlineValid is True:
            m = Milestone(assignment = Assignment.objects.get(title = "asdasd"), name = name,
                          deadline = formatDateTime)
            m.save()

            count = 0

            for task_id in got_tasks:

                task_from_id = Task.objects.get(id=task_id)
                m.tasks_to_complete.add(task_from_id)


                if task_from_id.completed is True:
                      count += 1


            # set progress
            progress = count / task_count
            if progress == 1.0:
                m.complete = True
                m.save()

            m.progress = progress

            m.save()

    context = {'tasks': tasks,
               'validDeadline': deadlineValid}
    return render(request, 'profiles/addMilestoneModal.html', context)

# viewing a milestone, allows the completion of tasks
def viewmilestone(request, mil_name):
    # get all activities
    activities = Activity.objects.all()

    # to filter out relevant activities
    activitiesToShow = []


    m = Milestone.objects.get(name = mil_name)

    tasks = m.tasks_to_complete.all()

    for activity in activities:
        for task in tasks:
            if task in activity.associated_tasks.all():
                activitiesToShow.append(activity)




    context = {'activities': activitiesToShow,
               'tasks': tasks,
               'milestone': m}
    return render(request, 'profiles/viewMilestoneModal.html', context)