from django.urls import path
from . import views

urlpatterns = [
    path('<int:user_id>', views.dashboard, name='dashboard'),
    path('do/<act_name>', views.doactivity, name='doactivity'),
    path('add', views.addactivity, name='addactivity'),
    path('addm', views.addmilestone, name='addmilestone'),
    path('viewm/<mil_name>', views.viewmilestone, name='viewmilestone')
]
