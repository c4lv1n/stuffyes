from django.db import models
from datetime import datetime
from django.contrib.auth.models import User


class Profile(models.Model):
    account = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    created_at = models.DateTimeField(default=datetime.now)

    # Create a module for given profile
    def add_module(self, name):
        from modules.models import Module

        # Check that duplicate modules aren't made within profile
        if not Module.objects.filter(profile=self, name=name).exists():
            Module.objects.create(profile=self, name=name)




    def add_activity(self, name):
        from activities.models import Activity


        if not Activity.objects.filter(profile=self, name=name).exists():
            Activity.objects.create(profile=self, name=name)



    def __str__(self):
        return self.title
