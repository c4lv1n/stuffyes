from django.db import models
from datetime import datetime
from modules.models import Module
from constants.types import ASSIGNMENT_TYPES


class Assignment(models.Model):
    module = models.ForeignKey(Module, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    assignment_type = models.CharField(max_length=50, choices=ASSIGNMENT_TYPES)
    deadline = models.DateTimeField()
    weighting = models.IntegerField(default=0)

    # Calculate time spent on an assignment
    @property
    def time_spent(self):
        from tasks.models import Task

        time = 0
        for task in Task.objects.filter(assignment=self):
            time += task.time_spent

        return time

    def __str__(self):
        return self.title
