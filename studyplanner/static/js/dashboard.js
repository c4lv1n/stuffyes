// import "./jquery-3.2.1";

// Toggle profile tabs with a select
$("#profile-select").on("change", e => {
  const selected = $("#profile-select option:selected").attr("data-target");
  $('.nav-tabs a[href="' + selected + '"]').tab("show");
});

// // Toggle extra info tabs
$("ul.nav-pills li a").click(function(e) {
  $("ul.nav-pills li.active").removeClass("active");
  $(this)
    .parent("li")
    .addClass("active");
});

// Calendar back button
$("a.la").on("click", e => {
  e.preventDefault();
  const tabs = $(".tab-content .active .calendar-area .nav-tabs a");
  const $selected = tabs.filter(".active");
  const index = tabs.index($selected);
  const next = (tabs.length + index - 1) % tabs.length;

  // Show previous month
  $(".tab-content .active .calendar-area .nav-tabs a:eq(" + next + ")").tab(
    "show"
  );
});

// Calendar forward button
$("a.ra").on("click", e => {
  e.preventDefault();
  const tabs = $(".tab-content .active .calendar-area .nav-tabs a");
  const $selected = tabs.filter(".active");
  const index = tabs.index($selected);
  const next = (index + 1) % tabs.length;

  // Show previous month
  $(".tab-content .active .calendar-area .nav-tabs a:eq(" + next + ")").tab(
    "show"
  );
});
