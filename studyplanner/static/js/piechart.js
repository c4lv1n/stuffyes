// Credit to code.tutsplus.com for the basis

// Draws a segment of the pie chart
function drawPieSlice(ctx, x, y, radius, startAngle, endAngle, col) {
  ctx.fillStyle = col;
  ctx.beginPath();
  ctx.moveTo(x, y);
  ctx.arc(x, y, radius, startAngle, endAngle);
  ctx.closePath();
  ctx.fill();
}

// Pie Chart object
const PieChart = function(options = null) {
  if (options) {
    this.options = options;
    this.canvas = options.canvas;
    this.ctx = this.canvas.getContext("2d");
    this.ctx.globalCompositeOperation = "xor";

    // Canvas dimensions
    this.canvas.width = 300;
    this.canvas.height = 300;

    // Constructs the pie chart
    this.draw = () => {
      let total = 0;
      this.options.data.forEach(mod => {
        total += mod.timespent;
      });

      // Handle a lack of data
      if (total == 0) {
        total = 1;
        this.options.data = [{ timespent: 1, colour: "rgba(150,150,150,0.5)" }];
      }

      let startangle = 0;
      this.options.data.forEach(mod => {
        time = mod.timespent;
        sliceangle = (2 * Math.PI * time) / total;

        drawPieSlice(
          this.ctx,
          this.canvas.width / 2,
          this.canvas.height / 2,
          Math.min(this.canvas.height / 2, this.canvas.width / 2),
          startangle,
          startangle + sliceangle,
          mod.colour
        );

        // Cut a transparent hole in the pie chart to make doughnut
        drawPieSlice(
          this.ctx,
          this.canvas.width / 2,
          this.canvas.height / 2,
          this.options.holesize *
            Math.min(this.canvas.height / 2, this.canvas.width / 2),
          0,
          2 * Math.PI,
          "#555"
        );

        startangle += sliceangle;
      });
    };
  }
};

// Variables to be used by template
let pieChart = new PieChart();
let data = [];
